git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init
> Inicia o git no diretório selecionado

git status
> Mostra as mudanças já feitas que ainda não tiveram commit

git add arquivo/diretório
> Adiciona as mudanças feitas no arquivo especificado à área de preparação do git

git add --all = git add .
> Adiciona as mudanças de todos os arquivos do repositório à área de preparação do git

git commit -m “Primeiro commit”
> Quando aliado às mudanças na área de preparação, salva as mudanças feitas no repositório local, junto com uma mensagem explicativa.
-------------------------------------------------------

git log
> Mostra o histórico de alterações de todo o repositório

git log arquivo
> Mostra o histórico de alterações do arquivo especificado

git reflog
> Mostra o histórico de alterações em um formato mais resumido
-------------------------------------------------------

git show
> Mostra as alterações feitas no último commit
git show <commit>
> Mostra as alterações feitas no commit especificado

-------------------------------------------------------

git diff
> Mostra o que foi adicionado ou deletado em comparação com o último commit
git diff <commit1> <commit2>
> Compara o que foi alterado entre 2 commits

-------------------------------------------------------

git reset --hard <commit>
> Desfaz todas as alterações feitas no commit especificado

-------------------------------------------------------

git branch
> Lista todas as branches locais

git branch -r 
> Lista todas as branches remotas

git branch -a
> Lista todas as branches remotas e locais

git branch -d <branch_name>
> Deleta uma branch (caso haja alterações não salvas, a branch não será deletada)

git branch -D <branch_name> 
> Deleta uma branch

git branch -m <nome_novo>
> Renomeia a branch atual para o nome especificado

git branch -m <nome_antigo> <nome_novo>
> Renomeia a branch especificada para o nome especificado

-------------------------------------------------------

git checkout <branch_name> 
> Troca a branch atual para a especificada

git checkout -b <branch_name> 
> Cria uma branch com o nome especificado e troca para a mesma

-------------------------------------------------------

git merge <branch_name> 
> Mescla as mudanças presentes na branch atual com a branch especificada

-------------------------------------------------------

git clone
> Cria uma cópia de um repositório existente em um novo diretório

git pull
> Atualiza o repositório local com a última versão do projeto

git push
> Atualiza o projeto com as alterações commitadas localmente

-------------------------------------------------------

git remote -v
> Lista os repositórios remotos que o repositório está usando

git remote add origin <url>
> Adiciona um servidor especificado origin à url especificada

git remote <url> origin
> Faz a troca da URL do servidor especificado origin para a URL especificada

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


